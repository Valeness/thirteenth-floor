package main

import (
	"fmt"
	"math/rand"
	"strconv"
	"sync"
	"time"
)



type Point struct {
	X int
	Y int
	DirectionX int
	DirectionY int
}

type Wolf struct {
	Position Point
	Energy int
	Hunger int
	Weight int
}

type Sheep struct {
	Position Point
	Weight int
}

func generateRandomPoint() (Point) {
	return Point{
		X : rand.Intn(40),
		Y : rand.Intn(40),
		DirectionX : rand.Intn(3) - 1,
		DirectionY : rand.Intn(3) - 1,
	}
}

func Move(Entity interface{}) {
	switch e := Entity.(type) {
		case Wolf:
		case Sheep:
			if e.Position.DirectionX == 1 {
				e.Position.X++
			} else if e.Position.DirectionX == 0 {
				e.Position.X--
			}

			if e.Position.DirectionY == 1 {
				e.Position.Y++
			} else if e.Position.DirectionY == 0 {
				e.Position.Y--
			}
	}
}

func GenerateWolf() (Wolf) {
	return Wolf{
		Energy : rand.Intn(100),
		Hunger : rand.Intn(100),
		Weight : 110 - rand.Intn(40),
		Position: generateRandomPoint(),
	}
}

func GenerateSheep() (Sheep) {
	return Sheep{
		Weight : 80 - rand.Intn(30),
		Position : generateRandomPoint(),
	}
}

func abs(number int) (int) {
	if(number < 0) {
		return number * -1
	}

	return number;
}

func HandleWolf() {
	for {
		select {
		case wolf := <- Wolves:
			wolf.Hunger += rand.Intn(3)
			if(wolf.Hunger > 30) {
				// Start the Prowl
				Move(wolf)

				//
				//multiplier := 1
				//
				//log.Println(wolf.Position.X, wolf.Position.Y)
				multiplier := 1
				//wolf.Position.DirectionX = 0
				//wolf.Position.DirectionY = -1
				//fmt.Println(wolf.Position.DirectionX, wolf.Position.DirectionY)
				//for multiplier := 1; multiplier < 4; multiplier++ {

				view_distance := 9

				if wolf.Position.DirectionY == 0 && wolf.Position.DirectionX != 0 {
					for x := 1; x < (view_distance / 2) + 2; x++ {
						lookingX := strconv.Itoa(wolf.Position.X + (x * wolf.Position.DirectionX))
						for y := -1 * multiplier; y < multiplier; y++ {
							lookingY := strconv.Itoa(wolf.Position.Y + (y ))
							fmt.Println("(" + lookingX + "," + lookingY + ")")
						}
						multiplier++
					}
					//os.Exit(1)
				} else if wolf.Position.DirectionX == 0 && wolf.Position.DirectionY != 0 {
					for y := 1; y < (view_distance / 2) + 2; y++ {
						lookingY := strconv.Itoa(wolf.Position.Y + (y * wolf.Position.DirectionY))
						for x := -1 * multiplier; x < multiplier; x++ {
							lookingX := strconv.Itoa(wolf.Position.X + (x))
							fmt.Println("(" + lookingX + "," + lookingY + ")")
						}
						multiplier++
					}
					//os.Exit(1)
				} else if wolf.Position.DirectionX != 0 && wolf.Position.DirectionY != 0 {

					direction_x := wolf.Position.DirectionX;
					direction_y := wolf.Position.DirectionY;
					x_start := wolf.Position.DirectionX
					x_end := view_distance * wolf.Position.DirectionX


					for x := x_start; abs(x) < abs(x_end); x += direction_x {
						//fmt.Println(x_start, x_end, direction_x)

						y_start := direction_y
						y_end := (view_distance + 1) - abs(x)

						for y := y_start; abs(y) < y_end; y += direction_y {
							lookingX := wolf.Position.X + x
							lookingY := wolf.Position.Y + y
							fmt.Println("(", lookingX, ",", lookingY, ")")
						}

					}
					//os.Exit(1)
				}
				//}

				//fmt.Println("====================================")

			}

			if wolf.Hunger > 80 {
				// Wolf dies
			}

			wg.Done()
		}
	}
}

func HandleSheep() {
	for {
		select {
		case sheep := <- SheepG:
			Move(sheep)
			wg.Done()
		}
	}
}

var Wolves = make(chan *Wolf, 0)
var SheepG = make(chan *Sheep, 0)
var wg = sync.WaitGroup{}

func main() {
	rand.Seed(time.Now().UnixNano())
	wolves := []*Wolf{}
	sheepg := []*Sheep{}

	for i := 0; i < 5; i++ {
		wolf := GenerateWolf()
		wolves = append(wolves, &wolf)
	}

	for i := 0; i < 10; i++ {
		sheep := GenerateSheep()
		sheepg = append(sheepg, &sheep)
	}

	for i := 0; i < 1; i++ {
		go HandleWolf()
		go HandleSheep()
	}

	ticks := 0

	for ticks < 1 {
		wg.Add(len(wolves))
		wg.Add(len(sheepg))
		for _, wolf := range wolves {
			Wolves <- wolf
		}

		for _, sheep := range sheepg {
			SheepG <- sheep
		}

		ticks++
		wg.Wait()
	}

}